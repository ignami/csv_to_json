# CSV To JSON

This is a simple script to convert a CSV file given in input to a JSON file in
output. The CSV file should have the headers in the first row. The JSON file is
composed by an array of object, each object corresponding to a row. The keys
are the header names.

## Requirements

- `git`.
- `poetry` (<https://python-poetry.org/docs/#installation>).

## Installation

1. Clone the repository.
1. Navigate to the `csv_to_json` directory.
1. Install the dependencies with `poetry install`.

Of course, this is not mandatory to use `poetry`. You can handle yourself your
own virtual environment and install the dependencies the way you want. Just
look for it in the `pyproject.toml` file.

## Usage

`poetry run python csv_to_json --help` displays a short help.

`poetry run python csv_to_json --input <file-path> --output <file-path>` will
convert the file given in input (CSV) to the file given in output (JSON).
