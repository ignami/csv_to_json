import click
import csv
import json

@click.command()
@click.option('--input', help='File path of the CSV file.')
@click.option('--output', default='output.json',
                          help='File path of the JSON file.')
def csv_to_json(input, output):
    """Convert a CSV file into JSON."""
    jsonArray = []

    # Read csv file
    with open(input, encoding='utf-8') as csvf:
        # Load csv file data using csv library's dictionnary reader
        csvReader = csv.DictReader(csvf)

        # Convert each csv row into python dict
        for row in csvReader:
            # Add this python dict to json array
            jsonArray.append(row)

        # Convert python jsonArray to JSON String and write to file
        with open(output, 'w', encoding='utf-8') as jsonf:
            jsonString = json.dumps(jsonArray, indent=4)
            jsonf.write(jsonString)

csv_to_json()
